package main

import (
	"fiber-mongo-api/configs"
	"fiber-mongo-api/routes"
	"github.com/gofiber/fiber/v2"
)

func main() {
	app := fiber.New()
	//run database
	configs.ConnectDB()

	//routes
	routes.FileRoute(app)

	app.Listen(":6000")
}
