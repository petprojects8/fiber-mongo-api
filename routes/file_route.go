package routes

import (
	"fiber-mongo-api/controllers"
	"github.com/gofiber/fiber/v2"
)

func FileRoute(app *fiber.App) {
	app.Post("/file", controllers.CreateFile)
	app.Get("/file/:fileId", controllers.GetAFile)
	app.Put("/file/:fileId", controllers.EditAFile)
	app.Delete("/file/:fileId", controllers.DeleteAFile)
	app.Get("/files", controllers.GetAllFile)
}
