package models

import "go.mongodb.org/mongo-driver/bson/primitive"

type File struct {
	Id   primitive.ObjectID `json:"id,omitempty"`
	Name string             `json:"name,omitempty" validate:"required"`
	Text string             `json:"text,omitempty" validate:"required"`
}
