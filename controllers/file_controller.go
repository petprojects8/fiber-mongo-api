package controllers

import (
	"context"
	"fiber-mongo-api/configs"
	"fiber-mongo-api/models"
	"fiber-mongo-api/responses"
	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"net/http"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

var fileCollection *mongo.Collection = configs.GetCollection(configs.DB, "files")
var validate = validator.New()

func CreateFile(c *fiber.Ctx) error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	var file models.File
	defer cancel()

	//validate the request body
	if err := c.BodyParser(&file); err != nil {
		return c.Status(http.StatusBadRequest).JSON(responses.FileResponse{Status: http.StatusBadRequest, Message: "error", Data: &fiber.Map{"data": err.Error()}})
	}

	//use the validator library to validate required fields
	if validationErr := validate.Struct(&file); validationErr != nil {
		return c.Status(http.StatusBadRequest).JSON(responses.FileResponse{Status: http.StatusBadRequest, Message: "error", Data: &fiber.Map{"data": validationErr.Error()}})
	}

	newFile := models.File{
		Id:   primitive.NewObjectID(),
		Name: file.Name,
		Text: file.Text,
	}

	result, err := fileCollection.InsertOne(ctx, newFile)
	if err != nil {
		return c.Status(http.StatusInternalServerError).JSON(responses.FileResponse{Status: http.StatusInternalServerError, Message: "error", Data: &fiber.Map{"data": err.Error()}})
	}

	return c.Status(http.StatusCreated).JSON(responses.FileResponse{Status: http.StatusCreated, Message: "success", Data: &fiber.Map{"data": result}})
}

func GetAFile(c *fiber.Ctx) error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	fileId := c.Params("fileId")
	var file models.File
	defer cancel()

	objId, _ := primitive.ObjectIDFromHex(fileId)

	err := fileCollection.FindOne(ctx, bson.M{"id": objId}).Decode(&file)
	if err != nil {
		return c.Status(http.StatusInternalServerError).JSON(responses.FileResponse{Status: http.StatusInternalServerError, Message: "error", Data: &fiber.Map{"data": err.Error()}})
	}

	return c.Status(http.StatusOK).JSON(responses.FileResponse{Status: http.StatusOK, Message: "success", Data: &fiber.Map{"data": file}})
}

func EditAFile(c *fiber.Ctx) error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	fileId := c.Params("fileId")
	var file models.File
	defer cancel()

	objId, _ := primitive.ObjectIDFromHex(fileId)

	//validate the request body
	if err := c.BodyParser(&file); err != nil {
		return c.Status(http.StatusBadRequest).JSON(responses.FileResponse{Status: http.StatusBadRequest, Message: "error", Data: &fiber.Map{"data": err.Error()}})
	}

	//use the validator library to validate required fields
	if validationErr := validate.Struct(&file); validationErr != nil {
		return c.Status(http.StatusBadRequest).JSON(responses.FileResponse{Status: http.StatusBadRequest, Message: "error", Data: &fiber.Map{"data": validationErr.Error()}})
	}

	update := bson.M{"name": file.Name, "title": file.Text}

	result, err := fileCollection.UpdateOne(ctx, bson.M{"id": objId}, bson.M{"$set": update})
	if err != nil {
		return c.Status(http.StatusInternalServerError).JSON(responses.FileResponse{Status: http.StatusInternalServerError, Message: "error", Data: &fiber.Map{"data": err.Error()}})
	}

	//get updated file details
	var updatedFile models.File
	if result.MatchedCount == 1 {
		err := fileCollection.FindOne(ctx, bson.M{"id": objId}).Decode(&updatedFile)
		if err != nil {
			return c.Status(http.StatusInternalServerError).JSON(responses.FileResponse{Status: http.StatusInternalServerError, Message: "error", Data: &fiber.Map{"data": err.Error()}})
		}
	}

	return c.Status(http.StatusOK).JSON(responses.FileResponse{Status: http.StatusOK, Message: "success", Data: &fiber.Map{"data": updatedFile}})
}

func DeleteAFile(c *fiber.Ctx) error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	fileId := c.Params("fileId")
	defer cancel()

	objId, _ := primitive.ObjectIDFromHex(fileId)

	result, err := fileCollection.DeleteOne(ctx, bson.M{"id": objId})
	if err != nil {
		return c.Status(http.StatusInternalServerError).JSON(responses.FileResponse{Status: http.StatusInternalServerError, Message: "error", Data: &fiber.Map{"data": err.Error()}})
	}

	if result.DeletedCount < 1 {
		return c.Status(http.StatusNotFound).JSON(
			responses.FileResponse{Status: http.StatusNotFound, Message: "error", Data: &fiber.Map{"data": "File with specified ID not found!"}},
		)
	}

	return c.Status(http.StatusOK).JSON(
		responses.FileResponse{Status: http.StatusOK, Message: "success", Data: &fiber.Map{"data": "File successfully deleted!"}},
	)
}

func GetAllFile(c *fiber.Ctx) error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	var files []models.File
	defer cancel()

	results, err := fileCollection.Find(ctx, bson.M{})

	if err != nil {
		return c.Status(http.StatusInternalServerError).JSON(responses.FileResponse{Status: http.StatusInternalServerError, Message: "error", Data: &fiber.Map{"data": err.Error()}})
	}

	//reading from the db in an optimal way
	defer results.Close(ctx)
	for results.Next(ctx) {
		var singleFile models.File
		if err = results.Decode(&singleFile); err != nil {
			return c.Status(http.StatusInternalServerError).JSON(responses.FileResponse{Status: http.StatusInternalServerError, Message: "error", Data: &fiber.Map{"data": err.Error()}})
		}

		files = append(files, singleFile)
	}

	return c.Status(http.StatusOK).JSON(
		responses.FileResponse{Status: http.StatusOK, Message: "success", Data: &fiber.Map{"data": files}},
	)
}
