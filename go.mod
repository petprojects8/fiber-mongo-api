module fiber-mongo-api

go 1.13

require (
	github.com/form3tech-oss/jwt-go v3.2.5+incompatible
	github.com/go-playground/validator/v10 v10.11.0
	github.com/gofiber/fiber/v2 v2.34.0
	github.com/gofiber/jwt/v2 v2.2.7
	github.com/joho/godotenv v1.4.0
	github.com/stretchr/testify v1.7.0
	go.mongodb.org/mongo-driver v1.9.1
	golang.org/x/crypto v0.0.0-20220214200702-86341886e292
	gopkg.in/asaskevich/govalidator.v9 v9.0.0-20180315120708-ccb8e960c48f
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
